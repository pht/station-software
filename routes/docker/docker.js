const express = require('express');
const router = express.Router();
const request = require('request');
const Train = require('../../models/Train');
const TransactionLog = require('../../models/TransactionLog')
const { getAgentOptions } = require('../../dind-certs-client');
const MetadataNotificationHelper = require('../helpers/MetadataNotifier')
const { utility, vault, trainConfigUtil, cryptoUtil } = require('../../utils');
const dockerContainerChangesKind = { 0: "Modified", 1: "Added", 2: "Deleted" };
const fs = require('fs');
const path = require('path');
const requestPromise = require("request-promise");
const tar = require("tar");
const tarStream = require("tar-stream");
const { Readable } = require("stream");
const { spawn } = require("child_process");
const diff2Html = require('diff2html');
const os = require('os');

const getTempDir = (dirPath, empty) => {

    // empty : if it already exists, remove all old files

    const tempDirPath = path.join(os.tmpdir(), dirPath);

    try {

        fs.mkdirSync(tempDirPath, { recursive: true });
        return tempDirPath;

    } catch (error) {

        if (error.code === 'EEXIST') {
            console.log(`already exists`);

            if (empty) {

                try {

                    fs.rmdirSync(tempDirPath, { recursive: true });
                    fs.mkdirSync(tempDirPath, { recursive: true });

                } catch (error) {
                    console.log(error);
                    return null
                }
            }
            
            return tempDirPath;
        }
        else {
            console.log(error);
            return null
        }
    }
}

const removeJobTempDir = (jobId) => {

    const tempDirPath = getTempDir(jobId);
    fs.rmdirSync(tempDirPath, { recursive: true });

}

const removeContainerTempDir = (jobId, container) => {

    const tempDirPath = getTempDir(path.join(jobId, container));
    fs.rmdirSync(tempDirPath, { recursive: true });

}

const getJobTempDir = (jobId, empty) => {

    const tempDirPath = getTempDir(jobId, empty);
    return tempDirPath;

}

const getTempContainerTarArchiveFileName = () => {

    const tempContainerTarArchiveFileName = "container.tar";
    return tempContainerTarArchiveFileName;

}

const getTempContainerTarArchiveSubPath = (jobId, container, beforeOrAfterExecution) => {

    console.log(jobId, container, beforeOrAfterExecution)

    var executionStateDirName = null;
    if (beforeOrAfterExecution === "before") {
        executionStateDirName = "before"
    }
    else if (beforeOrAfterExecution === "after") {
        executionStateDirName = "after"
    }

    const tempContainerTarArchiveFilePath = path.join(jobId, container, executionStateDirName);
    return tempContainerTarArchiveFilePath;

}

const getEncryptedTarArchiveFileName = () => {

    const encryptedTarArchiveFileName = "file.enc";
    return encryptedTarArchiveFileName;

}

const getEncryptedTarArchiveFilePathInContainer = () => {

    const encryptedTarArchiveFileName = getEncryptedTarArchiveFileName();
    const encryptedTarArchiveFilePathInContainer = path.join("/", encryptedTarArchiveFileName);
    return encryptedTarArchiveFilePathInContainer;
    
}

const getPrevChangesFileName = () => {

    const prevChangesFileName = "changes";
    return prevChangesFileName;

}

router.get('/image/pull', async function (req, res) {
    const { jobid, trainstoragelocation, trainclassid, currentstation, nextstation } = req.query;
    var auth = {
        username: [Censored],
        password: [Censored],
        email: [Censored]
    }
    var authInfo = Buffer.from(JSON.stringify(auth)).toString('base64');
    var options = {
        uri: 'https://' + process.env.DOCKER_HOST + ':' + process.env.DOCKER_PORT + '/images/create?fromImage=' + trainstoragelocation + '&tag=' + currentstation,
        method: 'POST',
        headers: {
            "Content-Type": "application/json",
            'X-Registry-Auth': authInfo
        },
        json: {},
        agentOptions: getAgentOptions()
    };

    let body = Buffer.from("");
    request(options).on("error", async (err) => {
        console.log('error: ', err);
        req.flash('error_msg', err);
        res.status(500).send();
    }).on("data", async (data) => {
        body = Buffer.concat([body, data])
        res.write(data);
    }).on("complete", async (response) => {
        console.log("complete");
        if (response.statusCode == 200) {

            try {

                // save pulled job information on local db
                const train = {
                    "jobid": jobid,
                    "trainstoragelocation": trainstoragelocation,
                    "trainclassid": trainclassid,
                    "currentstation": currentstation,
                    "nextstation": nextstation
                };
                // Insert or Update - If a job with the provided jobid exists, it will update - update: When a job visit the station more than once
                await Train.findOneAndUpdate(
                    { "jobid": jobid },
                    train,
                    {
                        upsert: true // Make this update into an upsert
                    });

                // log
                TransactionLog.create({ user: req.user.email, logMessage: "Image " + String(trainstoragelocation) + ":" + String(currentstation) + "pulled" });

                // metadata service
                MetadataNotificationHelper.notifyMetadataProviderImageFinishedDownloading(jobid, new Date());


                // create an empty temp directory - later is used for inspect changes, encryption, and decryption
                const tempDirPath = getJobTempDir(jobid, true);

                req.flash('success_msg', 'Image ' + trainstoragelocation + ':' + currentstation + ' has been pulled.');
                res.end();

            } catch (err) {
                console.log('error: ', err);
                req.flash('error_msg', err);
                res.end();
            }

        } else {
            req.flash('error_msg', (body.toString("utf-8")) || "Internal Server Error");
            console.log(body.toString("utf-8"));
            res.end()
        }
    });
    MetadataNotificationHelper.notifyMetadataProviderImageStartedDownloading(jobid, trainstoragelocation + ':' + currentstation, new Date());
});


// Reject Train requests
router.post('/train/reject', async function(req, res) {
    const { jobid, reason, comment} = req.body;
    let errors = [];
    if (reason === "0") {
        errors.push({ msg: 'Please select a reject reason.' });
    }
    if (reason === "3" && comment === "") {
        errors.push({ msg: 'Please enter the text of reject reason.' });
    }
    if (errors.length > 0) {
        res.render('dashboard-reject', {user: req.user, jobid: jobid, errors: errors});
    } else {
        let rejectMessage = comment;
        if (reason === "1"){
            rejectMessage = "No anonymity"
        }
        if (reason === "2"){
            rejectMessage = "No access right";
        }
        let options = {
            url: `https://${process.env.CENTRALSERVICE_ADDRESS}:${process.env.CENTRALSERVICE_PORT}/${process.env.CENTRALSERVICE_ENDPOINT}/hook`,
            headers: {
                'Content-Type': 'application/json',
                'authorization': [Censored],
            },
            body: JSON.stringify({
                type:"reject",
                jobId: jobid,
                rejectMessage: rejectMessage
            }),
        };
        request.post(options, function (error, response, body) {
            if (error) {
                req.flash('error_msg', error.message);
                res.redirect('/dashboard/reject');
            } 
            console.log(response.statusCode);
            console.log(response.body);
            TransactionLog.create({user:req.user.email,logMessage:"Train rejected with jobID:" + String(jobid)})
            MetadataNotificationHelper.notifyMetadataProviderImageRejected(jobid, rejectMessage, new Date());
            req.flash('success_msg', "The job "+jobid+" has been rejected.");
            res.redirect('/dashboard');
            
        });
    }
    
});


// Create Container
router.post('/container/create', utility.asyncHandler(async (req, res, next) => {

    const { jobId, image, envs, bindMount, shmSize } = req.body;
    
    // console.log(envs)

    let env_array = [];
    let env_array_manual = [];
    let env_array_vault = [];
    let env_array_vault_temp = [];

    //Separate manual type env variables from valut type env variables
    envs.forEach(item => {

        item.name = item.name.trim();

        if (item.type === "manual") {
            if (item.name.length < 1)
                return;
                
            item.value = item.value.trim();                                
            env_array_manual.push(`${item.name}=${item.value}`);
        }
        else if (item.type === "vault") {
            let temp = {
                path: JSON.parse(item.path).path,
                env: item.name,
                key: JSON.parse(item.path).key
            };
            env_array_vault_temp.push(temp);
        }
    });

    //Group the vault envs by their path
    env_array_vault_temp = utility.groupBy(env_array_vault_temp, 'path');


    // WRAPPED RESULT
    let uniquePaths = Object.keys(env_array_vault_temp);
    for (let index = 0; index < uniquePaths.length; index++) {
        const path = uniquePaths[index];
        // Get Response-Wrapping Token on path
        const response = await vault.read(path, { headers: { "X-Vault-Wrap-TTL": "1h" } });
        if (response.isError) {
            res.render('error', { user: req.user, error_msg: (JSON.stringify(response.data) || 'error') });
            return;
        }
        let temp = { path: response.wrap_info.token, kv: {} };
        env_array_vault_temp[path].forEach(x => {
            temp.kv[x.env] = x.key
        })
        env_array_vault.push(temp);
    }

    //Vault configs
    if (env_array_vault.length > 0) {

        let tokenConfig = {
            num_uses: env_array_vault.length
        };

        // console.log("GetTrainToken");
        const trainToken = await vault.getTrainToken(tokenConfig);
        // console.log("Token:", trainToken);
        if (trainToken.isError) {
            res.render('error', { user: req.user, error_msg: (JSON.stringify(trainToken.data) || 'error') });
            return;
        }

        env_array.push(`VAULT_ADDR=${await vault.getVaultApiEndpoint()}/sys/wrapping/unwrap`);
        env_array.push(`VAULT_TOKEN=${trainToken.auth.client_token}`);
        env_array.push(`VAULT_SECRET_CONFIG=${JSON.stringify(env_array_vault)}`);
    }

    env_array = env_array.concat(env_array_manual);
    // console.log(JSON.stringify(env_array));

    let options = {
        uri: `https://${process.env.DOCKER_HOST}:${process.env.DOCKER_PORT}/containers/create?name=${jobId}`,
        method: 'POST',
        headers: {
            "Content-Type": "application/json",
        },
        json: {
            "Image": image
        },
        agentOptions: getAgentOptions()
    };

    if (env_array.length) {
        options.json["Env"] = env_array;
    }

    options.json["HostConfig"] = {};

    if (bindMount.hostSource && bindMount.containerDestination) {
        options.json["HostConfig"] = {
            "Mounts":  [
                {
                    "Target": bindMount.containerDestination,
                    "Source": bindMount.hostSource,
                    "Type": "bind",
                    "ReadOnly": bindMount.readOnly == 'on'
                }
            ]
        };
    }

    if (shmSize && shmSize > 0) {
        options.json["HostConfig"].ShmSize = parseInt(shmSize);
    }

    

    request(options, (error, response, body) => {
        if (error) {
            console.error(error);
            req.flash('error_msg', error.message);
            res.redirect('/dashboard/images');
        }

        if (response.statusCode == 201) {

            // make a backup from the container before its execution - path: <os_temp_dir>/<job_id>/<container_id>/before/container.tar
            try {

                const container = body.Id.substring(0, 13);
                const tempContainerTarArchiveFileName = getTempContainerTarArchiveFileName();
                const tempContainerTarArchiveSubPath = getTempContainerTarArchiveSubPath(jobId, container, "before");
                const tempContainerTarArchiveFilePath = path.join(getTempDir(tempContainerTarArchiveSubPath, true), tempContainerTarArchiveFileName);

                let options = {
                    uri: `https://${process.env.DOCKER_HOST}:${process.env.DOCKER_PORT}/containers/${container}/export`,
                    method: 'GET',
                    agentOptions: getAgentOptions()
                };

                request(options).pipe(fs.createWriteStream(tempContainerTarArchiveFilePath)).on('finish', () => {

                    // console.log(body);
                    req.flash('success_msg', 'Container ' + container + ' has been created.');
                    TransactionLog.create({ user: req.user.email, logMessage: 'Container ' + container + ' created.' });
                    res.redirect('/dashboard/containers');

                });

            } catch (error) {

                console.log(error);
                req.flash('error_msg', JSON.stringify(error));
                res.redirect('/dashboard/images');

            }

        } else {
            console.log(body);
            req.flash('error_msg', JSON.stringify(body));
            res.redirect('/dashboard/images');
        }

    });
}));


router.get('/image/remove', function(req, res) {
    const { jobId, image } = req.query;

    var options = {
        uri: 'https://' + process.env.DOCKER_HOST + ':' + process.env.DOCKER_PORT + '/images/' + image + '?force=true',
        method: 'DELETE',
        agentOptions: getAgentOptions()
    };
    request(options, (error, response, body) => {
        if (error) {
            console.error(error);
            req.flash('error_msg', error.message);
            res.redirect('/dashboard/images');
        }
        if (response.statusCode == 200) {  
            console.log(body);
            req.flash('success_msg', 'Image ' + image + ' has been removed.');
            TransactionLog.create({user:req.user.email, logMessage:'Image ' + image + ' removed'});

            // remove temp direcotry
            removeJobTempDir(jobId);

            res.redirect('/dashboard/images');
        } else {
            console.error(body);
            req.flash('error_msg', body);
            res.redirect('/dashboard/images');
        }
    });
});


router.get('/image/push', function(req, res) {
    const { image } = req.query;
    var auth = {
        username: [Censored],
        password: [Censored],
        email: [Censored]
    }
    var authInfo = Buffer.from(JSON.stringify(auth)).toString('base64');
    var options = {
        uri: 'https://' + process.env.DOCKER_HOST + ':' + process.env.DOCKER_PORT + '/images/'+image+'/push',
        method: 'POST',
        headers: {
                "Content-Type": "application/json",
                'X-Registry-Auth': authInfo
        },
        json: {},
        agentOptions: getAgentOptions()
    };
    request(options, async (error, response, body) => {
        if (error) {
            console.error(error);
            req.flash('error_msg', error.message);
            res.redirect('/dashboard/images');
        }
        if (response.statusCode == 200) {  
            console.log("step1 - push image: ", body);

            // DELETE image after push (to Harbor)
            var options = {
                uri: 'https://' + process.env.DOCKER_HOST + ':' + process.env.DOCKER_PORT + '/images/' + image + '?force=true',
                method: 'DELETE',
                agentOptions: getAgentOptions()
            };
            request(options, (error, response, body) => {
                if (error) {
                    console.error(error);
                    req.flash('error_msg', error.message);
                    res.redirect('/dashboard/containers');
                }
                if (response.statusCode == 200) {  
                    console.log("step2 - remove image: ", body);
                    req.flash('success_msg', 'Image ' + image + ' has been pushed.');
                    TransactionLog.create({user:req.user.email, logMessage:'Image ' + image + ' pushed'});
                    res.redirect('/dashboard/images');
       
                } else {
                    console.error(body);
                    req.flash('error_msg', body);
                    res.redirect('/dashboard/containers');
                }
            });


        } else {
            console.error(body);
            req.flash('error_msg', body);
            res.redirect('/dashboard/images');
        }
    });
});


//Start Container
router.get('/container/start', function(req, res) {
    const { container, jobId } = req.query;
    // jobId may contain a leading /, dunno why
    // therefore, assumed that the jobId is just the pattern XXXXXX-XXXXX-XXXX-XXX
    const cleanedJobId = jobId.replace('/','')
    const options = {
        uri: 'https://'+process.env.DOCKER_HOST+':'+process.env.DOCKER_PORT+'/containers/' + container + '/start',
        method: 'POST',
        agentOptions: getAgentOptions() 
    };
    // The options for the wait request
    const waiting_options = {
        uri: 'https://' + process.env.DOCKER_HOST + ':' + process.env.DOCKER_PORT + '/containers/' + container + '/wait',
        method: 'POST',
        agentOptions: getAgentOptions()
    }
    request(options, (error, response, body) => {
        if (error) {
            console.error(error);
            req.flash('error_msg', error.message);
            res.redirect('/dashboard/images');
        }
        if (response.statusCode == 204) {  
            console.log(body);
            req.flash('success_msg', 'Container ' + container + ' has been started.');
            console.log('Container ' + container + ' started');
            MetadataNotificationHelper.notifyMetadataProviderImageStartedRunning(cleanedJobId, new Date());
            TransactionLog.create({user:req.user.email, logMessage:'Train started:' + String(cleanedJobId)})
            // waiting on the container and notify when it is stopeed:
            request(waiting_options, (error, response, body) => {
                if ( error ) {
                    console.error(error);
                    req.flash('error_msg', error.message);
                    return;
                }
                if(body.StatusCode == 0) {
                    MetadataNotificationHelper.notifyMetadataProviderImageFinished(cleanedJobId, true, new Date());
                } else {
                    MetadataNotificationHelper.notifyMetadataProviderImageFinished(cleanedJobId, false, new Date());
                }
            });
            res.redirect('/dashboard/containers');
        }else{
            console.error(body);
            req.flash('error_msg', body);
            res.redirect('/dashboard/images');
        }
    });
});



router.get('/container/remove', function (req, res) {
    const { jobId, container } = req.query;
    var options = {
        uri: 'https://' + process.env.DOCKER_HOST + ':' + process.env.DOCKER_PORT + '/containers/' + container,
        method: 'DELETE',
        agentOptions: getAgentOptions()
    };
    request(options, (error, response, body) => {
        if (error) {
            console.error(error);
            req.flash('error_msg', error.message);
            res.redirect('/dashboard/containers');
        }
        if (response.statusCode == 204) {
            console.log(body);
            req.flash('success_msg', 'Container ' + container + ' has been removed.');
            TransactionLog.create({ user: req.user.email, logMessage: 'Container ' + container + ' removed' });

            // remove temp direcotry
            removeContainerTempDir(jobId, container);
            
            res.redirect('/dashboard/containers');
        } else {
            console.error(body);
            req.flash('error_msg', body);
            res.redirect('/dashboard/containers');
        }
    });
});

router.get('/container/logs', function (req, res) {
    //container_id
    const { container } = req.query;
    var options = {
        uri: `https://${process.env.DOCKER_HOST}:${process.env.DOCKER_PORT}/containers/${container}/logs?stdout=true&stderr=true`,
        method: 'GET',
        agentOptions: getAgentOptions()
    };

    let body = Buffer.from("");
    request(options).on("error", async (err) => {
        console.log('error: ', err);
        req.flash('error_msg', err);
        res.status(500).send();
    }).on("data", async (data) => {
        // Remove header
        if (data[1] == 00 && data[2] == 00 && data[3] == 00)
            data = data.slice(8, data.length);
        // console.log("data", data.toString("utf-8"));
        body = Buffer.concat([body, data]);
    }).on("complete", async (response) => {
        // console.log("complete");
        // console.log(body.toString("utf-8"));
        res.json(JSON.stringify(body.toString("utf-8")));
    });
});

router.get('/container/changes', function (req, res) {
    const { container } = req.query;
    var options = {
        uri: `https://${process.env.DOCKER_HOST}:${process.env.DOCKER_PORT}/containers/${container}/changes`,
        method: 'GET',
        agentOptions: getAgentOptions()
    };
    request(options, (error, response, body) => {
        if (error) {
            console.error(`error: ${error}`);
            res.status(500).send(error);
        }
        if (response.statusCode == 200) {
            try {
                let changes = JSON.parse(body);

                let tree = [];
                let level = { tree };

                if (changes) {
                    changes = changes.map(x => { x.KindName = dockerContainerChangesKind[x.Kind]; return x });

                    changes.forEach(item => {
                        let path = item.Path;
                        path.split('/').reduce((r, name, i, a) => {
                            if (!r[name]) {
                                r[name] = { tree: [] };
                                r.tree.push({ name, children: r[name].tree, path: (name ? path : ""), kindCode: item.Kind, kindName: item.KindName, containerId: container });
                            }

                            return r[name];
                        }, level)
                    });
                }

                res.json(JSON.stringify(tree));

            } catch (error) {
                console.error(error);
                req.flash('error_msg', error);
                res.redirect('/dashboard/containers');
            }
        } else {
            console.error(`error: ${body}`);
            res.status(500).send(body);
        }
    });
});

router.get('/container/compare', utility.asyncHandler(async (req, res, next) => {
    const { jobId, container, compareFilePath } = req.query;

    const tempContainerTarArchiveFileName = getTempContainerTarArchiveFileName();

    const tempContainerTarArchiveSubPath_beforeExecution = getTempContainerTarArchiveSubPath(jobId, container, "before");
    const tempContainerTarArchiveSubPath_afterExecution = getTempContainerTarArchiveSubPath(jobId, container, "after");

    const tempContainerTarArchiveDirPath_beforeExecution = getTempDir(tempContainerTarArchiveSubPath_beforeExecution);
    const tempContainerTarArchiveDirPath_afterExecution = getTempDir(tempContainerTarArchiveSubPath_afterExecution);

    const tempContainerTarArchiveFilePath_beforeExecution = path.join(tempContainerTarArchiveDirPath_beforeExecution, tempContainerTarArchiveFileName);
    const tempContainerTarArchiveFilePath_afterExecution = path.join(tempContainerTarArchiveDirPath_afterExecution, tempContainerTarArchiveFileName);

    try {

        // GET CONTAINER CHANGES
        let containerChangesOptions = {
            uri: `https://${process.env.DOCKER_HOST}:${process.env.DOCKER_PORT}/containers/${container}/changes`,
            method: 'GET',
            agentOptions: getAgentOptions()
        };        
        const containerChangesResult = await requestPromise(containerChangesOptions);
        const containerChangesResultJson = JSON.parse(containerChangesResult) || [];

        // Exclude sub directories - KEEP FILES ONLY
        let fileList = [];
        let filePathList = [];
        containerChangesResultJson.forEach(file => {
            let index = fileList.findIndex(x => { return file.Path.includes(x.Path) });
            if (index < 0)
                fileList.push(file)
            else
                fileList.splice(index, 1, file)
        });

        if (compareFilePath) {
            console.log(`File List: ${JSON.stringify(fileList)}`);
            console.log(`File path: ${compareFilePath}`);
            fileList = fileList.filter(file => file.Path === compareFilePath);
            console.log(`New File List: ${JSON.stringify(fileList)}`);
        }

        filePathList = fileList.map(x => x.Path.substring(1));
        // console.log(filePathList);

        // Extract changes from before execution backup
        if (fs.existsSync(tempContainerTarArchiveFilePath_beforeExecution)) {
            // Extract only changed files - before execution version
            tar.x({
                cwd: tempContainerTarArchiveDirPath_beforeExecution,
                file: tempContainerTarArchiveFilePath_beforeExecution,
                sync: true
            }, filePathList);
        }
        else {
            throw new Error('Could not find backup file (before execution).');
        }

        // Download and extract (piped) changed files from the executed container
        let options = {
            uri: `https://${process.env.DOCKER_HOST}:${process.env.DOCKER_PORT}/containers/${container}/export`,
            method: 'GET',
            agentOptions: getAgentOptions()
        };
        await new Promise((resolve, reject) => {
            request(options).pipe(tar.extract({
                cwd: tempContainerTarArchiveDirPath_afterExecution,
                sync: true,
            }, filePathList))
                .on('finish', resolve)
                .on('error', (error) => {
                    console.log(error);
                    reject(error);
                });
        });
        
        const binaryFiles = /^Binary files (.*) and (.*) differ/;
        const binaryHead = ['--- ','+++ ','@@ -0 +0 @@'].join('\n');

        let promises = [];
        filePathList.forEach(file => {

            const path_to_file_in_before_execution_dir = path.join(tempContainerTarArchiveDirPath_beforeExecution, file);
            const path_to_file_in_after_execution_dir = path.join(tempContainerTarArchiveDirPath_afterExecution, file);

            // Execute 'diff' command 
            const cmd = `diff --new-file -u ${path_to_file_in_before_execution_dir} ${path_to_file_in_after_execution_dir}`;
            const cmdArray = cmd.split(' ');

            promises.push(
                new Promise((resolve, reject) => {

                    let stdout = "";
                    let stderr = "";
                    const proc = spawn(cmdArray[0], cmdArray.slice(1));

                    proc.stdout.on('data', (data) => {
                        if (binaryFiles.test(data))
                            data = binaryHead.concat('\n', data);
                        stdout = stdout.concat(data);
                    });

                    proc.stderr.on('data', (data) => {
                        stderr = stderr.concat(data);
                    });

                    proc.on('close', (code) => {
                        // Exit code is 0 if inputs are the same, 1 if different, 2 if trouble.
                        if (code === 2) {
                            console.log(cmd);
                            console.log(`stdout: ${stdout}`);
                            console.log(`stderr: ${stderr}`);
                            reject();
                        }
                        else {
                            resolve(stdout);
                        }
                    });

                    proc.on('error', (err) => {
                        console.log(err)
                        reject();
                    });

                })
            );
            
        });

        let promisesResults = await Promise.all(promises);
        // console.log(promisesResults);

        const diffUnifiedString = promisesResults.join('\n');
        // console.log(diffUnifiedString);

        const diffJson = diff2Html.parse(diffUnifiedString);
        // console.log(JSON.stringify(diffJson))

        // Hide the line according to its type (Only new added lines ('insert') are displayed, the rest ('delete', 'context') are replaced with '***confidential***') 
        diffJson.forEach(diff => {
            // strip base path from file name
            diff.oldName = diff.oldName.replace(tempContainerTarArchiveDirPath_beforeExecution, '');
            diff.newName = diff.oldName;

            let index = fileList.findIndex(x => diff.newName.includes(x.Path));
            // 0: "Modified", 1: "Added", 2: "Deleted"
            if (fileList[index].Kind === 1)
                diff.isNew = true
            else if (fileList[index].Kind === 2)
                diff.isDeleted = true

            diff.blocks.forEach(block => {
                block.lines.forEach(line => {

                    // LineType {
                    //     'insert',
                    //     'delete',
                    //     'context',
                    // }

                    if (line.type === "insert") {
                        return
                    }
                    else {
                        line.content = `${line.content[0]}***confidential***`
                    }
                })
            })
        });

        if (compareFilePath) {
            res.send(diffJson);
        } else {
            const diffHtml = diff2Html.html(diffJson);
            res.end(diffHtml);
        }
    } catch (error) {
        console.error(`error: ${error}`);
        res.status(500).send(error);
    }
}));

router.get('/container/archive', utility.asyncHandler(async (req, res, next) => {
    const { container, path } = req.query;
 
    // ***** TO-DO : path should be authorized - Only added files can be downloaded *****

    let options = {
        uri: `https://${process.env.DOCKER_HOST}:${process.env.DOCKER_PORT}/containers/${container}/archive?path=${path}`,
        method: 'GET',
        agentOptions: getAgentOptions()
    };

    request(options).on("error", async (err) => {
        console.log('error: ', err);
        res.status(500).send(err);
    }).on("response", async (response) => {

        if (response.statusCode == 200) {
            let fileName = `${container}_${path.replaceAll("/", "_")}.tar`;
            res.set('Content-disposition', 'attachment; filename=' + fileName);
            res.set('Content-Type', 'application/x-tar');
        } else {
            let fileName = "error.json";
            res.set('Content-disposition', 'attachment; filename=' + fileName);
            res.set('Content-Type', 'application/json');
        }
    }).on("data", async (data) => {
        res.write(data);
    }).on("complete", async (response) => {
        res.end();
    });

}));

router.get('/container/file', utility.asyncHandler(async (req, res) => {
    const { jobId, container, compareFilePath } = req.query;

    const tempContainerTarArchiveSubPath_beforeExecution = getTempContainerTarArchiveSubPath(jobId, container, "before");
    const tempContainerTarArchiveSubPath_afterExecution = getTempContainerTarArchiveSubPath(jobId, container, "after");

    const tempContainerTarArchiveDirPath_beforeExecution = getTempDir(tempContainerTarArchiveSubPath_beforeExecution);
    const tempContainerTarArchiveDirPath_afterExecution = getTempDir(tempContainerTarArchiveSubPath_afterExecution);

    try {
        let options = {
            uri: `https://${process.env.DOCKER_HOST}:${process.env.DOCKER_PORT}/containers/${container}/archive?path=${compareFilePath}`,
            method: 'GET',
            agentOptions: getAgentOptions()
        };
        await new Promise((resolve, reject) => {
            request(options).pipe(tar.extract({
                cwd: tempContainerTarArchiveDirPath_afterExecution,
                sync: true,
            }))
            .on('finish', resolve)
            .on('error', (error) => {
                console.log(error);
                reject(error);
            });
        });
        const filePath = compareFilePath.split('/');
        const pathToFileInAfterExecutionDir = path.join(tempContainerTarArchiveDirPath_afterExecution, filePath[filePath.length - 1]);
        const fileContents = fs.readFileSync(pathToFileInAfterExecutionDir);
        res.send(fileContents);
    } catch (error) {
        console.error(`error: ${error}`);
        res.status(500).send(error);
    }
}));

// Commit Container - NEW - WITH ENCRYPTION
router.get('/container/commit', utility.asyncHandler(async (req, res, next) => {

    const { jobId, image, container, repo, tag,  } = req.query;

    const tempDirPath = getJobTempDir(jobId);
    const encryptedTarArchiveFileName = getEncryptedTarArchiveFileName();
    const encryptedTarArchiveFilePathInContainer = getEncryptedTarArchiveFilePathInContainer();

    try {

        // check vault health status
        let vaultHealthStatus = await vault.healthStatus();

        // This includes a list of files that have been modified by previous stations - They are decrypted and must be encrypted again before the train leaves the station
        let prevChanges = [];
        const prevChangesFileName = getPrevChangesFileName();
        const prevChangesFilePath = path.join(tempDirPath, prevChangesFileName);

        if (fs.existsSync(prevChangesFilePath)) {
            console.log("prev_changes");
            const fileContent = fs.readFileSync(prevChangesFilePath);
            prevChanges = JSON.parse(fileContent);
        }
        else {
            throw new Error('Could not find backup file. Please (re)decrypt the image first.');
        }

        // GET a list of changes made by this run
        let containerChangesOptions = {
            uri: `https://${process.env.DOCKER_HOST}:${process.env.DOCKER_PORT}/containers/${container}/changes`,
            method: 'GET',
            agentOptions: getAgentOptions()
        };    
        const containerChangesResult = await requestPromise(containerChangesOptions);
        const containerChangesResultJson = JSON.parse(containerChangesResult) || [];

        // exclude subdirectories (sub changes) from the list of changes - KEEP ONLY FILES
        let fileList = [];
        let deletedFileList = [];
        containerChangesResultJson.forEach(file => {
            // 0: "Modified", 1: "Added", 2: "Deleted"
            // ignore deleted files - deleted files are lated deleted from the base image
            if (file.Kind === 2) {
                
                let index = prevChanges.findIndex(x => {
                    // if needed, consider "/" in comparison
                    if (file.Path.startsWith("/")) {
                        x = `/${x}`;
                    }
                    return x.startsWith(file.Path);
                });

                // two types of deletion: 1.deleted file is part of the previous changes 2.deleted file is part of the base image
                if (index > -1) {
                    // remove deleted file from prev changes
                    prevChanges.splice(index, 1);
                }
                else {
                    deletedFileList.push(file.Path);
                }
                return;
            }

            let index = fileList.findIndex(x => { return file.Path.includes(x.Path) });
            if (index < 0)
                fileList.push(file)
            else
                fileList.splice(index, 1, file)
        });

        // strip "/" from paths - first character
        let fileListStripped = [];
        fileListStripped = fileList.map(x => {
            if (x.Path.startsWith("/"))
                return x.Path.substring(1);
        });
        console.log(fileListStripped);

        // append prev changes to current changes
        fileListStripped = prevChanges.concat(fileListStripped);

        // keep unique paths
        fileListStripped = [ ...new Set(fileListStripped)];  


        const tempDirSubPathAfterContainerExecution = getTempContainerTarArchiveSubPath(jobId, container, "after");
        const tempDirPathAfterContainerExecution = getTempDir(tempDirSubPathAfterContainerExecution);
        const containerTarArchiveFileName = getTempContainerTarArchiveFileName();
        const containerTarArchiveFilePath = path.join(tempDirPathAfterContainerExecution, containerTarArchiveFileName);
        
        // download and extract (piped) changed files from the container
        let options = {
            uri: `https://${process.env.DOCKER_HOST}:${process.env.DOCKER_PORT}/containers/${container}/export`,
            method: 'GET',
            agentOptions: getAgentOptions()
        };

        await new Promise(resolve => {
            request(options).pipe(tar.extract({
                cwd: tempDirPathAfterContainerExecution,
                sync: true,
            }, fileListStripped))
                .on('finish', resolve)
                .on('error', (error) => {
                    console.log(error);
                    reject(error);
                });
        });
    
        // create a new tar archive contains all changes
        const tarArchiveFileName = "file.tar";
        const tarArchiveFilePath = path.join(tempDirPathAfterContainerExecution, tarArchiveFileName);

        // No files have changed
        if (!fileListStripped.length) {
            // An empty tar file is a file with 10240 NUL bytes
            // NUL '\u0000'
            const emptyTar = Array(10240).fill('\u0000').join('');
            fs.writeFileSync(tarArchiveFilePath, emptyTar);
        }
        else {
            tar.create({
                cwd: tempDirPathAfterContainerExecution,
                file: tarArchiveFilePath,
                sync: true,
            }, fileListStripped);
        }

        // encode tar file to base64 - needed for vault encryption
        let tarFileBase64Encoded = Buffer.from(fs.readFileSync(tarArchiveFilePath)).toString('base64');

        // generate a symmetric key
        const symmetricKeyName = jobId;
        let generateSymmetricKeyResult = await vault.command.write(`transit/keys/${symmetricKeyName}`, {
            exportable: true,
            allow_plaintext_backup: true,
            type: "aes256-gcm96"
        });

        // encrypt changes with the symmetric key
        let encryptionResult = await vault.command.write(`transit/encrypt/${symmetricKeyName}`, { plaintext: tarFileBase64Encoded });

        // write ciphertext in a file
        const encryptedTarArchiveFilePath = path.join(tempDirPathAfterContainerExecution, encryptedTarArchiveFileName);
        fs.writeFileSync(encryptedTarArchiveFilePath, encryptionResult.data.ciphertext);

        // export the symmetric key
        let exportSymmetricKeyResult = await vault.command.read(`/transit/export/encryption-key/${symmetricKeyName}/1`);
        const symmetricKey = exportSymmetricKeyResult.data.keys['1'];

        // FIND base Image Id
        // 1. inspect container for its image
        let inspectContainerOptions = {
            uri: `https://${process.env.DOCKER_HOST}:${process.env.DOCKER_PORT}/containers/${container}/json`,
            method: 'GET',
            agentOptions: getAgentOptions()
        };
        const inspectContainerResult = await requestPromise(inspectContainerOptions);
        const inspectContainerResultJson = JSON.parse(inspectContainerResult);

        // 2. inspect history of image for its base image
        const imageId = inspectContainerResultJson.Image;
        let imageHistoryOptions = {
            uri: `https://${process.env.DOCKER_HOST}:${process.env.DOCKER_PORT}/images/${imageId}/history`,
            method: 'GET',
            agentOptions: getAgentOptions()
        };
        const imageHistoryResult = await requestPromise(imageHistoryOptions);
        const imageHistoryResultJson = JSON.parse(imageHistoryResult);
        const baseImageId = imageHistoryResultJson.reverse().find(item => item.Id !== "<missing>").Id;        

        // Dockerfile
        dockerFileLines = [
            `FROM ${baseImageId}`
        ];

        // COPY file.enc
        dockerFileLines = dockerFileLines.concat([`COPY ${encryptedTarArchiveFileName} ${encryptedTarArchiveFilePathInContainer}`]);

        // update train_config file
        let trainConfig = trainConfigUtil.getTrainConfigJsonBaseModel();
        const trainConfigFileName = trainConfigUtil.getTrainConfigFileName();
        const trainConfigFilePath = path.join(tempDirPath, trainConfigFileName);
        if (fs.existsSync(trainConfigFilePath)) {
            console.log("train_config");
            const fileContent = fs.readFileSync(trainConfigFilePath);
            trainConfig = JSON.parse(fileContent);
        }
        trainConfig = await trainConfigUtil.updateTrainConfigJson(trainConfig, symmetricKey);
        const updatedTrainConfigFilePath = path.join(tempDirPathAfterContainerExecution, trainConfigFileName);
        fs.writeFileSync(updatedTrainConfigFilePath, JSON.stringify(trainConfig));

        const trainConfigFilePathInContainer = trainConfigUtil.getTrainConfigFilePathInContainer();
        // COPY train_config.json 
        dockerFileLines = dockerFileLines.concat([`COPY ${trainConfigFileName} ${trainConfigFilePathInContainer}`]);

        // dockerFileLines.push(`LABEL train_config=${JSON.stringify(JSON.stringify(trainConfig))}`);
        
        // REMOVE files - If a file is removed in this execution, remove it from base image
        dockerFileLines = dockerFileLines.concat(deletedFileList.map( filePath => `RUN rm -rf ${filePath}`));

        // save Dockerfile
        const dockerFileName = "Dockerfile";
        const dockerFilePath = path.join(tempDirPathAfterContainerExecution, dockerFileName);
        fs.writeFileSync(dockerFilePath, dockerFileLines.join('\n'));

        // create tar archive file to build a new image
        const encImageTarArchiveFileName = "encImage.tar";
        const encImageTarArchiveFilePath = path.join(tempDirPathAfterContainerExecution, encImageTarArchiveFileName);
        tar.create({
            cwd: tempDirPathAfterContainerExecution,
            file: encImageTarArchiveFilePath,
            sync: true,
        }, [encryptedTarArchiveFileName, dockerFileName, trainConfigFileName]);
    
        // build a new image (contains updated train_config.json and file.enc)
        const t = `${repo}:${tag}`
        console.log(t);
        let buildOptions = {
            uri: `https://${process.env.DOCKER_HOST}:${process.env.DOCKER_PORT}/build?t=${t}`,
            method: 'POST',
            agentOptions: getAgentOptions()
        };

        buildOptions.body = fs.createReadStream(encImageTarArchiveFilePath);
        const buildResult = await requestPromise(buildOptions);
        console.log(buildResult);

        // DELETE container after encryption and building a new image
        let deleteContainerOptions = {
            uri: `https://${process.env.DOCKER_HOST}:${process.env.DOCKER_PORT}/containers/${container}`,
            method: 'DELETE',
            agentOptions: getAgentOptions()
        };
        let deleteContainerResult = await requestPromise(deleteContainerOptions);

        // DELETE IMAGE
        let deleteImageOptions = {
            uri: `https://${process.env.DOCKER_HOST}:${process.env.DOCKER_PORT}/images/${imageId}?force=true`,
            method: 'DELETE',
            agentOptions: getAgentOptions()
        };
        let deleteImageResult = await requestPromise(deleteImageOptions);

        TransactionLog.create({user:req.user.email, logMessage:'Container ' + container + ' committed'});

        // remove temp directory
        removeJobTempDir(jobId);

        req.flash('success_msg', 'Container ' + container + ' has been encrypted and committed.');
        res.redirect('/dashboard/images');

    } catch (error) {
        console.log(error);
        req.flash('error_msg', utility.stringifyErrorMsg(error));
        res.redirect('/dashboard/containers');
    }

}));

router.get('/image/decrypt', utility.asyncHandler(async (req, res, next) => {

    const { jobId, image } = req.query;

    const tempDirPath = getJobTempDir(jobId);
    const encryptedTarArchiveFileName = getEncryptedTarArchiveFileName();
    const encryptedTarArchiveFilePathInContainer = getEncryptedTarArchiveFilePathInContainer();

    let tempContainerId = null;
    let extractPath = null;

    // list of files added to the base image on decryption process
    let changes = [];
    // train_config
    let trainConfig = trainConfigUtil.getTrainConfigJsonBaseModel();

    try {
        console.log("try1");

        // check vault health status
        let vaultHealthStatus = await vault.healthStatus();

        // create a temp container
        let createTempContainerOptions = {
            uri: `https://${process.env.DOCKER_HOST}:${process.env.DOCKER_PORT}/containers/create`,
            method: 'POST',
            json: {
                "Image": image
            },
            agentOptions: getAgentOptions()
        };
        let createTempContainerResult = await requestPromise(createTempContainerOptions);
        tempContainerId = createTempContainerResult.Id;

        extractPath = getTempDir(path.join(jobId, tempContainerId), true);

        try {

            console.log("try2");

            // HEAD - check the existence of the train_config file
            let trainConfigFilePathInContainer = trainConfigUtil.getTrainConfigFilePathInContainer();
            let trainConfigFileHeadArchiveOptions = {
                uri: `https://${process.env.DOCKER_HOST}:${process.env.DOCKER_PORT}/containers/${tempContainerId}/archive?path=${trainConfigFilePathInContainer}`,
                method: 'HEAD',
                agentOptions: getAgentOptions()
            };
            let trainConfigFileHeadArchiveResult = await requestPromise(trainConfigFileHeadArchiveOptions);

            // GET - download and extract train_config file from the temp container (docker api provides tar archive)
            let trainConfigFileGetArchiveOptions = {
                uri: `https://${process.env.DOCKER_HOST}:${process.env.DOCKER_PORT}/containers/${tempContainerId}/archive?path=${trainConfigFilePathInContainer}`,
                method: 'GET',
                agentOptions: getAgentOptions(),
                transform: function (body) {
                    return Readable.from([body]);
                }
            };
            let trainConfigFileGetArchiveResult = await requestPromise(trainConfigFileGetArchiveOptions);

            // untar train_config
            trainConfig = await trainConfigUtil.unTarTrainConfigJson(trainConfigFileGetArchiveResult);

            if (trainConfig.hasOwnProperty(trainConfigUtil.train_config_constant["symmetric_key"]) && trainConfig[trainConfigUtil.train_config_constant["symmetric_key"]]) {

                const encryptedSymmetricKey = trainConfig[trainConfigUtil.train_config_constant["symmetric_key"]];
                const decryptedSymmetricKey = await trainConfigUtil.decryptSymmetricKey(encryptedSymmetricKey);
                const symmetricKeyBackup = cryptoUtil.getVaultSymmetricKeyBackupModel(jobId, decryptedSymmetricKey, null, { isBase64: true });

                // restore symmetric key to vault
                const symmetricKeyName = jobId;
                let restoreSymmetricKeyResult = await vault.command.write(`transit/restore/${jobId}`, {
                    backup: symmetricKeyBackup,
                    name: symmetricKeyName,
                    // force the restore to proceed even if a key by this name already exists.
                    force: true
                });

                // HEAD - check the existence of the encrypted file
                let headArchiveOptions = {
                    uri: `https://${process.env.DOCKER_HOST}:${process.env.DOCKER_PORT}/containers/${tempContainerId}/archive?path=${encryptedTarArchiveFilePathInContainer}`,
                    method: 'HEAD',
                    agentOptions: getAgentOptions()
                };
                let headArchiveResult = await requestPromise(headArchiveOptions);

                // GET - download and extract encrypted file from the temp container (docker api provides tar archive)
                await new Promise((resolve, reject) => {

                    let options = {
                        uri: `https://${process.env.DOCKER_HOST}:${process.env.DOCKER_PORT}/containers/${tempContainerId}/archive?path=${encryptedTarArchiveFilePathInContainer}`,
                        method: 'GET',
                        agentOptions: getAgentOptions()
                    };

                    let stream = request(options)
                        .pipe(
                            tar.extract({
                                cwd: extractPath,
                                sync: true,
                            })
                        )
                        .on('finish', () => {
                            resolve();
                        })
                        .on('error', (error) => {
                            console.log(error);
                            reject(error);
                        })
                }).catch(error => {
                    console.log(error);
                    reject(error);
                });

                // read ciphertext
                const encryptedTarArchiveFilePath = path.join(extractPath, encryptedTarArchiveFileName);
                const ciphertext = fs.readFileSync(encryptedTarArchiveFilePath, "utf8");

                // decrypt
                const decryptionResult = await vault.write(`transit/decrypt/${symmetricKeyName}`, { ciphertext: ciphertext });
                const plaintextEncoded = decryptionResult.data.plaintext;

                // base64 decode - a tar archive file
                const plaintext = Buffer.from(plaintextEncoded, 'base64');

                // List the contents of a tar archive
                const plaintextStream = Readable.from(plaintext);
                let extractListOfContents = tarStream.extract();
                extractListOfContents.on('entry', function (header, stream, next) {

                    // header is the tar header
                    // stream is the content body (might be an empty stream)
                    // call next when you are done with this entry

                    // clear stream - just file name is enough
                    if (header.type == "file") {
                        changes.push(header.name);
                        stream = Readable.from("");
                    }

                    stream.on('end', function () {
                        next() // ready for next entry
                    })

                    stream.resume(); // just auto drain the stream
           
                }).on('finish', function () {
                    // all entries read
                    
                });
                plaintextStream.pipe(extractListOfContents);

                // put decrypted file into the container
                let putArchiveOptions = {
                    uri: `https://${process.env.DOCKER_HOST}:${process.env.DOCKER_PORT}/containers/${tempContainerId}/archive?path=/`,
                    method: 'PUT',
                    agentOptions: getAgentOptions(),
                    body: plaintext
                };
                let putArchiveResult = await requestPromise(putArchiveOptions);

            }

        } catch (error) {
            console.log("catch2");            
            console.log("Decryption requirements are not complete.");

            console.log(error);
            req.flash('error_msg', utility.stringifyErrorMsg(error));
            res.redirect('/dashboard/images');
            return;
        }

        // COMMIT temp container
        // set repo and tag from image name
        const lastIndexOfColon = image.lastIndexOf(":");
        const repo = image.substring(0, lastIndexOfColon);
        const tag = image.substring(lastIndexOfColon + 1);

        let commitTempContainerOptions = {
            uri: `https://${process.env.DOCKER_HOST}:${process.env.DOCKER_PORT}/commit?container=${tempContainerId}&repo=${repo}&tag=${tag}`,
            method: 'POST',
            headers: {
                "Content-Type": "application/json",
            },
            json: {
                "Labels": {
                    "decrypted": "true",
                },
            },
            agentOptions: getAgentOptions()
        };
        let commitTempContainerResult = await requestPromise(commitTempContainerOptions);

        req.flash('success_msg', 'Image ' + image + ' has been decrypted.');
        res.redirect('/dashboard/images');

    } catch (error) {
        console.log("catch1");

        console.log(error);
        req.flash('error_msg', utility.stringifyErrorMsg(error));
        res.redirect('/dashboard/images');

    } finally {

        console.log("finally");

        if (tempContainerId) {
            // DELETE temp container
            var deleteTempContainerOptions = {
                uri: `https://${process.env.DOCKER_HOST}:${process.env.DOCKER_PORT}/containers/${tempContainerId}`,
                method: 'DELETE',
                agentOptions: getAgentOptions()
            };
            request(deleteTempContainerOptions, (error, response, body) => {
                if (error) {
                    console.error(error);
                }
                if (body) {
                    console.error(body);
                }
            });
        }

        if (extractPath) {
            // DELETE extraction directory
            fs.rm(extractPath, { recursive: true, force: true }, (err) => {
                if (err)
                    console.log(err)
            });
        }

        // store list of decrypted files
        const prevChangesFileName = getPrevChangesFileName();
        const prevChangesFilePath = path.join(tempDirPath, prevChangesFileName);
        fs.writeFileSync(prevChangesFilePath, JSON.stringify(changes));

        const trainConfigFileName = trainConfigUtil.getTrainConfigFileName();
        const trainConfigFilePath = path.join(tempDirPath, trainConfigFileName);
        fs.writeFileSync(trainConfigFilePath, JSON.stringify(trainConfig));

    }
}));


// ***** TEMP CODE - DOWNLOAD IMAGE AS A TAR ARCHIVE FILE - START *****
router.get('/image/export', utility.asyncHandler(async (req, res, next) => {
    const { image } = req.query;
    let options = {
        uri: `https://${process.env.DOCKER_HOST}:${process.env.DOCKER_PORT}/images/${image}/get`,
        method: 'GET',
        agentOptions: getAgentOptions()
    };

    request(options).on("error", async (err) => {
        console.log('error: ', err);
        res.status(500).send(err);
    }).on("response", async (response) => {

        if (response.statusCode == 200) {
            let fileName = `${image}.tar`;
            res.set('Content-disposition', 'attachment; filename=' + fileName);
            res.set('Content-Type', 'application/x-tar');
        } else {
            let fileName = "error.json";
            res.set('Content-disposition', 'attachment; filename=' + fileName);
            res.set('Content-Type', 'application/json');
        }
    }).on("data", async (data) => {
        res.write(data);
    }).on("complete", async (response) => {
        res.end();
    });
}));
// ***** TEMP CODE - DOWNLOAD IMAGE AS A TAR ARCHIVE FILE - END *****


// ***** TEMP CODE - DOWNLOAD CONTANIER AS A TAR ARCHIVE FILE - START *****
router.get('/container/export', utility.asyncHandler(async (req, res, next) => {
    const { container } = req.query;
    let options = {
        uri: `https://${process.env.DOCKER_HOST}:${process.env.DOCKER_PORT}/containers/${container}/export`,
        method: 'GET',
        agentOptions: getAgentOptions()
    };

    request(options).on("error", async (err) => {
        console.log('error: ', err);
        res.status(500).send(err);
    }).on("response", async (response) => {

        if (response.statusCode == 200) {
            let fileName = `${container}.tar`;
            res.set('Content-disposition', 'attachment; filename=' + fileName);
            res.set('Content-Type', 'application/x-tar');
        } else {
            let fileName = "error.json";
            res.set('Content-disposition', 'attachment; filename=' + fileName);
            res.set('Content-Type', 'application/json');
        }
    }).on("data", async (data) => {
        res.write(data);
    }).on("complete", async (response) => {
        res.end();
    });
}));
// ***** TEMP CODE - DOWNLOAD CONTANIER AS A TAR ARCHIVE FILE - END *****

module.exports = router;