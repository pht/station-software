// Environment variable config
require('dotenv').config();

// var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var bodyParser = require('body-parser');

const expressLayouts = require('express-ejs-layouts');
const passport = require('passport');
const flash = require('connect-flash');
const session = require('express-session');
const mongoose = require('mongoose');
const promiseRetry = require('promise-retry');

const config = require('./config/keys');
const initPassport = require('./validation/passport');

var indexRouter = require('./routes/index');

const Crypto = require('crypto')

function randomString(size = 21) {  
  return Crypto
    .randomBytes(size)
    .toString('base64')
    .slice(0, size)
}

var app = express();

// Passport Config
initPassport(passport);

// Connect to MongoDB - START
const mongoURI = config.mongoURI;
console.log(mongoURI);

// https://jira.mongodb.org/browse/NODE-1643
const mongooseConnectOptions = {
  useNewUrlParser: true,
  maxPoolSize: 10,
  useUnifiedTopology: true

}

const promiseRetryOptions = {
  retries: 60,
  factor: 1.5,
  minTimeout: 1000,
  maxTimeout: 5000
}

const connectToMongoDB = (url) => {
  return promiseRetry((retry, number) => {
    console.log(`MongoClient connecting to ${url} - retry number: ${number}`)
    return mongoose.connect(url, mongooseConnectOptions).catch(retry)
  }, promiseRetryOptions)
}

connectToMongoDB(mongoURI);
// Connect to MongoDB - END

// view engine setup
app.set('views', path.join(__dirname, './views'));
app.set('view engine', 'ejs');
app.use(expressLayouts);

// Express body parser
app.use(express.urlencoded({ extended: true }));

// Express session
app.use(
  session({
    secret: process.env.SESSION_SECRET || randomString(32),
    resave: true,
    saveUninitialized: true
  })
);


// Passport middleware
app.use(passport.initialize());
app.use(passport.session());

// Connect flash
app.use(flash());

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, './assets')));

// parse application/json
app.use(bodyParser.json())

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  res.locals.success_msg = req.flash('success_msg');
  res.locals.error_msg = req.flash('error_msg');
  res.locals.error = req.flash('error');
  res.locals.logs = req.flash('logs');
  next();
});

app.use((err, req, res, next) => {
  res.status(err.status || 400).json({
    success: false,
    message: err.message || 'An error occured.',
    errors: err.error || [],
  });
});

app.use('/', indexRouter);

// error handler
// app.use(function(err, req, res, next) {
//   // set locals, only providing error in development
//   res.locals.message = err.message;
//   res.locals.error = req.app.get('env') === 'development' ? err : {};

//   // render the error page
//   res.status(err.status || 500);
//   res.render('error');
// });

module.exports = app;
