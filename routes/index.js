const express = require('express');
const router = express.Router();
const dashboardRouter = require('./dashboard/dashboard');
const authRouter = require('./auth/auth');
const apiRouter = require('./api/api');
const dockerRouter = require('./docker/docker');
const metadataRouter = require('./metadata/metadata')
const errorRouter = require('./error/error');
const vaultRouter = require('./vault/vault');


const {forwardAuthenticated, ensureAuthenticated} = require('../validation/auth');

// Welcome Page
router.get('/', forwardAuthenticated, (req, res) => res.render('welcome'));

// RESTful API router
router.use('/api', apiRouter);

// Dashboard
router.use('/dashboard', dashboardRouter);

// Authenfication
router.use('/auth', authRouter);

// Docker API
router.use('/docker', ensureAuthenticated, dockerRouter);


// Metadata service API
router.use('/metadata', ensureAuthenticated, metadataRouter)

// Vault
router.use('/vault', ensureAuthenticated, vaultRouter);

//Error
router.use('/error', errorRouter);


module.exports = router;