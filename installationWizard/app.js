const express = require('express');
const bodyParser = require('body-parser');
const routerFactory = require('./installationWizard')
const configurationManager = require('./configurationManager')
const expressLayouts = require('express-ejs-layouts');
const path = require('path');
//const vaultRouter = require('../routes/vault/vault')
//const flash = require('connect-flash');
//const session = require('express-session');

const app = express();

// view engine setup
app.set('views', path.join(__dirname, '../views'));
app.set('view engine', 'ejs');
app.use(expressLayouts);

// Express body parser
app.use(express.urlencoded({ extended: true }));

app.use(express.json());

app.use('/setup', routerFactory(configurationManager))

//app.use('/setup/vault', vaultRouter)

app.set('configurationManager', configurationManager)

app.get('/', (req, res) => {
  res.redirect('/setup/wizard?page=0')
})

module.exports = app