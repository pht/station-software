const crypto = require('crypto');
const fs = require('fs');
const algorithm = 'aes-256-ctr';

const encrypt = (buffer, key) => {
    // Create an initialization vector
    const iv = crypto.randomBytes(16);
    // Create a new cipher using the algorithm, key, and iv
    const cipher = crypto.createCipheriv(algorithm, key, iv);
    // Create the new (encrypted) buffer
    const result = Buffer.concat([iv, cipher.update(buffer), cipher.final()]);
    return result;
};

const decrypt = (encrypted, key) => {
    // Get the iv: the first 16 bytes
    const iv = encrypted.slice(0, 16);
    // Get the rest
    encrypted = encrypted.slice(16);
    // Create a decipher
    const decipher = crypto.createDecipheriv(algorithm, key, iv);
    // Actually decrypt it
    const result = Buffer.concat([decipher.update(encrypted), decipher.final()]);
    return result;
};

let func = null
try {
    func = process.argv[2];

} catch (error) {
    console.log(error);
}

if (func == "encrypt") {
    let stationOtp = "[Censored]";
    let envBuffer = fs.readFileSync("./env");
    hashKey = crypto.createHash('sha256').update(String(stationOtp)).digest('base64').substr(0, 32);
    const envBufferEncrypted = encrypt(envBuffer, hashKey);
    fs.writeFileSync("./env.enc", envBufferEncrypted);
}

if (func == "decrypt") {

    let envEncrypted = fs.readFileSync("./env.enc");
    let stationOtp = "[Censored]";
    let key = crypto.createHash('sha256').update(String(stationOtp)).digest('base64').substr(0, 32);

    const decrypted = decrypt(envEncrypted, key);
    console.log(decrypted.toString());

}