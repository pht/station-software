// exports factory 

const { decrypt, validateRsaKeyPair } = require('./token_crypt')
const { keyGenerator, EXPORT_OPTIONS_PRIVATE, EXPORT_OPTIONS_PUBLIC } = require('./keyGeneration')
const base64Util = require('../utils/base64');
const requestPromise = require('request-promise');
const { publicKey, privateKey } = require('./configurationManager');


const PAGES = ['installationWizard/page0','installationWizard/page1','installationWizard/page2','installationWizard/page3','installationWizard/page4']

// request options for keycloak authentication
getKeycloakAuthenticationRequestOptions = (username, password) => {

    let authServer = new URL('/auth/realms/pht/protocol/openid-connect/token', `https://${process.env.AUTH_SERVER_ADDRESS}`);
    authServer.port = process.env.AUTH_SERVER_PORT;
    let options = {
        method: 'POST',
        url: authServer.toString(),
        headers: {
            'Content-Type': 'application/json',
        },
        form: {
            grant_type: "password",
            client_id: "central-service",
            username: username,
            password: password,
            scope: "openid profile email offline_access",
        }
    };
    console.log(options);
    return options;
}

// request options to share public key with the central service
getSharingPublicKeyWithCenterRequestOptions = (accessToken, publicKey) => {

    // base64 encoding
    let publicKeyBase64 = base64Util.encode(publicKey);

    let endpoint = new URL(`/${process.env.CENTRALSERVICE_ENDPOINT}/api/stations/publickey`, `https://${process.env.CENTRALSERVICE_ADDRESS}`);
    endpoint.port = process.env.CENTRALSERVICE_PORT;

    let options = {
        method: 'POST',
        url: endpoint.toString(),
        headers: {
            'Content-Type': 'application/json',
            'authorization': `Bearer ${accessToken}`
        },
        body: JSON.stringify({
            publicKey: publicKeyBase64
        }),
    };

    console.log(options);
    return options;
}

// send the station public key with the central service
sharePublicKeyWithCenter = async (username, password, publicKey) => {

    try {

        // get an access token from the center
        const keycloakAuthReqOptions = getKeycloakAuthenticationRequestOptions(username, password);
        const keycloakAuthReqResult = await requestPromise(keycloakAuthReqOptions);
        const accessToken = JSON.parse(keycloakAuthReqResult)["access_token"];

        const sharingPublicKeyWithCenterReqOpts = getSharingPublicKeyWithCenterRequestOptions(accessToken, publicKey);
        const sharingPublicKeyWithCenterResult = await requestPromise(sharingPublicKeyWithCenterReqOpts);

        console.log(sharingPublicKeyWithCenterResult);

        return Promise.resolve();
    } catch (error) {
        console.log(error);
        return Promise.reject();
    }

}

validateCustomRsaKeyPair = async (publicKey, privateKey) => {
    try {
        await validateRsaKeyPair(publicKey, privateKey);
        return Promise.resolve();
    } catch (error) {
        return Promise.reject(error);
    }
}

module.exports = (configurationManager) => {
    // message queue
    messages = []
    // password to use for decryption
    otp = "" 
    

    // the view controller, renders the page of the setup specified with the page parameter
    async function wizardViewController (req, res) {
        // which page is wanted?
        // default: render page 0
        if(!('page' in req.query && req.query.page < PAGES.length)) {
            res.redirect('?page=0')
        }
        const options = {}
        // for page 2, we need to provide the temporary password and the username
        if (PAGES[req.query.page] == 'installationWizard/page2') {
            try {
                const { username, password } = configurationManager.getHarborUsernameAndPassword()
                options['username'] = username
                options['temp_password'] = password
            } catch (error) {
                console.log(error)
                console.log("A reason for this error can be that no env file was provided")
                options['username'] = "unavailable"
                options['temp_password'] = "unavailable"
            } 
                
        }
        res.render(PAGES[req.query.page], { page: Number(req.query.page), numofpages: Number(PAGES.length), ...options })
    }

    async function confupload(req, res) {
        messages.push("Upload Started...")
        if(!req.files) {
            return res.sendStatus(400)
        } else {
            const envfile = req.files.envfile
            // notify
            messages.push('File Uploaded with md-5 hash:' + String(envfile.md5))
            try {
                console.log("Start encrypting with otp:" + String(otp))
                decryptedVal = decrypt(envfile.data, otp)                  
                configurationManager.envconfiguration = decryptedVal.toString()
            } catch (e) {
                console.log(e)
                messages.push("Error while decrypting file")
                return res.sendStatus(400)
            }
            try {
                messages.push("Perform sanity checks on configuration...")
                await configurationManager.envconfigurationSanityCheck()
                messages.push("Sanity check complete.")
                messages.push("File decrypted")
            } catch(e) {
                console.log(e)
                messages.push("Error while file decryption. Is the password correct?")
                res.sendStatus(400)
                return
            }
            return res.sendStatus(200)
        }
    }

    async function setOtp (req, res) {
        if (!("otp" in req.body)) {
            res.sendStatus(400)
        }
        otp = req.body.otp
        messages.push("Password transmitted...")
        res.sendStatus(200)
    }
    // a hook for getting messages
    // simple but enough for this use case
    async function messagehook (req, res) {
        if (messages.length > 0) {
            const messageCopy = messages
            messages = []
            res.send({messages: messageCopy})
        } else {
            res.send({messages: []})
        }
    }

    async function setHarborPassword(req, res) {
        if(!("new_password" in req.body)) {
            res.sendStatus(400)
            return
        }
        const { new_password } = req.body;

        // validate the new password (try to log into the center(keycloak) with provided credentials)
        const { username } = configurationManager.getHarborUsernameAndPassword();
        let options = getKeycloakAuthenticationRequestOptions(username, new_password);
        try {
            let requestResult = await requestPromise(options);
            console.log(requestResult);
        } catch (error) {
            console.log(error);
            res.sendStatus(error.statusCode || 500);
            return
        }

        configurationManager.updateHarborPassword(new_password)
        res.sendStatus(200)
    }

    // a hook for generating a key-pair
    async function generateKeys(req, res) {
        // generate custom key pair here
        keyGenerator().then(async (key_res) => {
            const { pub, priv } = key_res
            configurationManager.publicKey = pub.export(EXPORT_OPTIONS_PUBLIC)
            configurationManager.privateKey = priv.export(EXPORT_OPTIONS_PRIVATE)

            // share the public key with the center
            try {
                const { username, password } = configurationManager.getHarborUsernameAndPassword();
                await sharePublicKeyWithCenter(username, password, configurationManager.publicKey);
            } catch (error) {
                console.log(error)
                return res.sendStatus(500)
            }

            res.send({publicKey: configurationManager.publicKey})
        })
    }

    // a hook for submitting own key p
    async function submitKeys(req, res) {
        if (!("privateKey" in req.body && "publicKey" in req.body)) {
            return res.sendStatus(400)
        }
        console.log(JSON.stringify(req.body))
        const { privateKey, publicKey } = req.body
        console.log("Received key-pair with public key:" + String(publicKey));

        try {
            await validateCustomRsaKeyPair(String(publicKey), String(privateKey));
        } catch (error) {
            console.log(error);
            return res.sendStatus(500);
        }

        configurationManager.publicKey = String(publicKey)
        configurationManager.privateKey = String(privateKey)

        // share the public key with the center
        try {
            const { username, password } = configurationManager.getHarborUsernameAndPassword();
            await sharePublicKeyWithCenter(username, password, configurationManager.publicKey);
        } catch (error) {
            console.log(error)
            return res.sendStatus(500);
        }

        return res.sendStatus(200);
    }

    async function configComplete(req, res) {
        console.log("Configuration did end...")
        configurationManager.configurationDidEnd()
        res.sendStatus(200)
    }

    return {
        confupload,
        wizardViewController,
        messagehook,
        setOtp,
        setHarborPassword,
        generateKeys,
        submitKeys,
        configComplete,
    }
}