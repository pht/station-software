const axios = require('axios')
const host = process.env.METADATAPROVIDER_ENDPOINT || "http://metadataprovider:9988"
const imageIdentifyingCache = []
async function notifyMetadataProviderImageStartedDownloading(jobId, fullImageIdentifier, timestamp) {
    console.log("USING HOST FOR METADATA NOTIFICATIONS:" + host)
    console.log("Sending StartedDownloading Notification for:" + jobId)
    if (!timestamp instanceof Date) {
        throw TypeError("timestamp is not date compatible type")
    }
    if (!jobId instanceof String) {
        throw TypeError("jobId is no string compatible type")
    }
    data = {
        jobId: jobId,
        image: fullImageIdentifier,
        timestamp: timestamp.toISOString()
    }
    await axios.post(host + '/execution/startedDownloading', JSON.stringify(data)).then(res => { console.log('Done with res:'+String(res.statusCode))}).catch(err => console.log(err.message))
}
async function notifyMetadataProviderImageFinishedDownloading(jobId, timestamp) {
    console.log("Sending FinishDownloading Notification for:" + jobId)
    if (!timestamp instanceof Date) {
        throw TypeError("timestamp is not date compatible type")
    }
    if (!jobId instanceof String) {
        throw TypeError("jobId is no string compatible type")
    }
    data = {
        jobId: jobId,
        timestamp: timestamp.toISOString()
    }
    return axios.post(host + '/execution/finishedDownloading', JSON.stringify(data)).then(res => { console.log('Done with res:'+String(res.statusCode))}).catch(err => console.log(err.message))
}
async function notifyMetadataProviderImageStartedRunning(jobId, timestamp) {
    console.log("Sending StartedRunning Notification for:" + jobId)
    if (!timestamp instanceof Date) {
        throw TypeError("timestamp is not date compatible type")
    }
    if (!jobId instanceof String) {
        throw TypeError("jobId is no string compatible type")
    }
    data = {
        jobId: jobId,
        timestamp: timestamp.toISOString()
    }
    return axios.post(host + '/execution/startedRunning', JSON.stringify(data)).then(res => { console.log('Done with res:'+String(res.statusCode))}).catch(err => console.log(err.message))
}
async function notifyMetadataProviderImageFinished(jobId, successful, timestamp) {
    console.log("Sending finish Notification for:" + jobId)
    if (!timestamp instanceof Date) {
        throw TypeError("timestamp is not date compatible type")
    }
    if (!jobId instanceof String) {
        throw TypeError("jobId is no string compatible type")
    }
    if (!jobId instanceof Boolean) {
        throw TypeError("successful is no string compatible type")
    }
    data = {
        jobId: jobId,
        timestamp: timestamp.toISOString(),
        successful: successful
    }
    return axios.post(host + '/execution/finished', JSON.stringify(data)).then(res => { console.log('Done with res:'+String(res.statusCode))}).catch(err => console.log(err.message))
}
async function notifyMetadataProviderImageRejected(jobId, message, timestamp) {
    console.log("Sending reject Notification for:" + jobId)
    if (!timestamp instanceof Date) {
        throw TypeError("timestamp is not date compatible type")
    }
    if (!jobId instanceof String) {
        throw TypeError("jobId is no string compatible type")
    }

    data = {
        jobId: jobId,
        timestamp: timestamp.toISOString(),
        message: message
    }
    return axios.post(host + '/execution/rejected', JSON.stringify(data)).then(res => { console.log('Done with res:'+String(res.statusCode))}).catch(err => console.log(err.message))
}

module.exports= {
    notifyMetadataProviderImageFinished,
    notifyMetadataProviderImageFinishedDownloading,
    notifyMetadataProviderImageRejected,
    notifyMetadataProviderImageStartedDownloading,
    notifyMetadataProviderImageStartedRunning,
    imageIdentifyingCache
}