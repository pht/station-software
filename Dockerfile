FROM node:latest as node_cache

# Create app directory
WORKDIR /usr/src/app

# Install app dependencies
# A wildcard is used to ensure both package.json AND package-lock.json are copied
# where available (npm@5+)
COPY package*.json ./

RUN npm install
# If you are building your code for production
# RUN npm ci --only=production

FROM node:latest

WORKDIR /usr/src/app

COPY --from=node_cache /usr/src/app/ .

# Bundle app source
COPY . .

RUN mkdir /lockfiledir

EXPOSE 3030
CMD [ "node", "./bin/www" ]
