const { response } = require('express');
const express = require('express');
const router = express.Router();
const { utility, vault } = require('../../utils');

//List All KV Engines - View
router.get('/', utility.asyncHandler(async (req, res, next) => {

    let vaultHealthStatus = await vault.command.health();
    // console.log(vaultHealthStatus);
    if (vaultHealthStatus.isError) {
        res.render('error', { user: req.user, error_msg: (JSON.stringify(vaultHealthStatus.data.error) || 'error')});
        return;
    }

    if (!vaultHealthStatus.initialized) {
        res.redirect('/vault/init/');
        return;
    }

    if (vaultHealthStatus.sealed) {
        res.redirect('/vault/unseal/');
        return;
    }

    let vaultIsAuthenticated = await vault.isAuthenticated();
    if (!vaultIsAuthenticated) {
        res.redirect('/vault/set-token/');
        return;
    }

    // create a default transit engine for encryption and decryption process
    let vaultTransitEngineIsEnabled = await vault.transitEngineIsEnabled();
    console.log(vaultTransitEngineIsEnabled);
    if(!vaultTransitEngineIsEnabled) {
        let enableTransitEngineResult = await vault.enableTransitEngine();
    }

    let kvEngines = await vault.getKeyValueEngines();
    // console.log(kvEngines);
    res.render('./vault/vault-kv-engine-list', { user: req.user, kvEngines: kvEngines});
}));

router.get('/init', utility.asyncHandler(async (req, res, next) => {

    let initStatus = await vault.command.initialized();
    if (initStatus.initialized) {
        res.redirect('/vault');
        return;
    }
    res.render('./vault/vault-init', { user: req.user });
}));

router.post('/init', utility.asyncHandler(async (req, res, next) => {

    const { keyShares, keyThreshold } = req.body;
    let initOption = {
        secret_shares : parseInt(keyShares),
        secret_threshold : parseInt(keyThreshold)
    }
    initResult = await vault.command.init(initOption);
    console.log(initResult);
    
    res.render('./vault/vault-init-download-keys', { user: req.user, vaultKeys: initResult, vaultkeyThreshold : keyThreshold });
}));

router.get('/unseal', utility.asyncHandler(async (req, res, next) => {

    let sealStatus = await vault.command.status();
    if (!sealStatus.initialized) {
        res.redirect('/vault');
        return;
    }
    if (!sealStatus.sealed) {
        res.redirect('/vault');
        return;
    }
    
    sealStatus.progressPercentage = ((sealStatus.progress/sealStatus.t) * 100).toFixed(2);
    res.render('./vault/vault-unseal', { user: req.user, vaultSealStatus: sealStatus });
}));

router.post('/unseal', utility.asyncHandler(async (req, res, next) => {

    const { key } = req.body;
    let unsealOption = {
        key : key
    }
    unsealResult = await vault.command.unseal(unsealOption);
    console.log("unsealResult",unsealResult);
    
    if (unsealResult.sealed) {
        res.redirect('/vault/unseal');
        return;
    }

    res.redirect('/vault');
}));

router.get('/set-token', utility.asyncHandler(async (req, res, next) => {

    let sealStatus = await vault.command.status();
    if (!sealStatus.initialized) {
        res.redirect('/vault');
        return;
    }
    if (sealStatus.sealed) {
        res.redirect('/vault');
        return;
    }

    let vaultIsAuthenticated = await vault.isAuthenticated();
    if (vaultIsAuthenticated) {
        res.redirect('/vault');
        return;
    }

    res.render('./vault/vault-set-token', { user: req.user});
}));

router.post('/set-token', (req, res, next) => {

    const { token } = req.body;
    vault.setToken(token);

    res.redirect('/vault');
});


//Enable KV Engine - View
router.get('/kv/enable', async (req, res, next) => {
    res.render('./vault/vault-kv-engine-enable', { user: req.user });
});

//Enable KV Engine
router.post('/kv/enable', utility.asyncHandler(async (req, res, next) => {

    const { path, version } = req.body;

    let vaultReqBody = { "path": path, "type": "kv", "config": {}, "options": { "version": version }, "generate_signing_key": true }
    let result = await vault.write(`sys/mounts/${path}`, vaultReqBody);
    // console.log(result);
    if (!result.isError) {
        res.redirect(`/vault/kv/secret/read/${encodeURIComponent(path + '/')}`);
    }
    else {
        req.flash('error_msg', JSON.stringify(result) || 'error');
        res.redirect('/vault/kv/enable');
    }
}));

//Disable KV Engine
router.get('/kv/disable/:kvEngine', utility.asyncHandler(async (req, res, next) => {

    let kvEngine = req.params.kvEngine;
    // console.log('kvEngine', kvEngine)

    let result = await vault.delete(`sys/mounts/${kvEngine}`);
    // console.log(result);
    if (result.isError) {
        req.flash('error_msg', JSON.stringify(result) || 'error');
    }
    else {
        req.flash('success_msg', `${kvEngine} successfully disabled.`);
    }
    res.redirect('/vault');
}));

//KV Engine Configuration - View
router.get('/kv/configuration/:kvEngine', utility.asyncHandler(async (req, res, next) => {

    let kvEngine = req.params.kvEngine;
    // console.log('kvEngine', kvEngine)

    let result = await vault.read(`sys/internal/ui/mounts/${kvEngine}`);
    // console.log(result);
    if (result.isError) {
        req.flash('error_msg', JSON.stringify(result) || 'error');
        res.redirect('/vault');
        return;
    }
    res.render('./vault/vault-kv-engine-config-show', { user: req.user, kvEngineConfig: result.data });

}));

router.get('/kv/secret/read/:vaultPath', utility.asyncHandler(async (req, res, next) => {

    let vaultPath = req.params.vaultPath;
    let vaultApiPath = vaultPath;
    // console.log('vaultPath', vaultPath);

    let kvEnginePath = vaultPath.split('/')[0];
    let kvEngineConfig = await vault.read(`sys/internal/ui/mounts/${kvEnginePath}`);

    if (kvEngineConfig.isError) {
        req.flash('error_msg', JSON.stringify(kvEngineConfig) || 'error');
        res.redirect('/vault');
        return;
    }

    //List sub path secrets
    if (vaultPath.endsWith('/')) {

        if (kvEngineConfig.data.options.version == 2) {
            let temp = vaultPath.split('/');
            temp.splice(1, 0, 'metadata');
            vaultApiPath = temp.join('/')
        }
        let result = await vault.list(vaultApiPath);
        // console.log('result', result);
        let paths = [];
        if (!result.isError) {
            paths = result.data.keys;
        }

        let vaultPathBreadcrumb = vaultPath.split('/').slice(0, -1).map(function (o, i) {
                return { "showName": o, "href": (vaultPath.split('/').slice(0, i + 1).concat('').join('/')) };
        });
        // console.log(vaultPathBreadcrumb);

        res.render('./vault/vault-kv-secret-list', { user: req.user, vaultPath: vaultPath, paths: paths, vaultPathBreadcrumb: vaultPathBreadcrumb });
    }
    //Show secret
    else {
        if (kvEngineConfig.data.options.version == 2) {
            let temp = vaultPath.split('/');
            temp.splice(1, 0, 'data');
            vaultApiPath = temp.join('/')
        }
        let result = await vault.read(vaultApiPath);
        // console.log('result', result);
        if (!result.isError) {
            let data = {};
            let metadata = null;
            if (kvEngineConfig.data.options.version == 2) {
                data = result.data.data;
                metadata = result.data.metadata;
            }
            else
                data = result.data;
            res.render('./vault/vault-kv-secret-show', { user: req.user, vaultPath: vaultPath, secretData: data, secretMetaData: metadata });
        }
        else {
            req.flash('error_msg', JSON.stringify(result) || 'error');
            res.redirect('/vault');
        }
    }

}));

router.get('/kv/secret/create/:vaultPath', async (req, res, next) => {

    let vaultPath = req.params.vaultPath;
    let vaultApiPath = vaultPath;
    // console.log('vaultPath', vaultPath);

    res.render('./vault/vault-kv-secret-create', { user: req.user, vaultPath: vaultPath });
});

router.post('/kv/secret/create', utility.asyncHandler(async (req, res, next) => {

    const { vaultPath, secretPath, secretData } = req.body;

    let vaultReqBody = {};

    secretData.forEach(secret => {
        if (secret.key)
            vaultReqBody[secret.key] = secret.value;
    });

    let vaultApiPath = `${vaultPath}${secretPath}`;
    // console.log(vaultReqBody);
    let result = await vault.write(vaultApiPath, vaultReqBody);
    // console.log(result);
    if (!result.isError) {
        res.redirect(`/vault/kv/secret/read/${encodeURIComponent(vaultApiPath)}`);
    }
    else {
        req.flash('error_msg', JSON.stringify(result) || 'error');
        res.redirect(`/vault/kv/secret/create/${encodeURIComponent(vaultPath)}`);
    }

}));

router.get('/kv/secret/delete/:vaultPath', utility.asyncHandler(async (req, res, next) => {

    let vaultPath = req.params.vaultPath;
    // console.log('vaultPath', vaultPath);

    let result = await vault.delete(`${vaultPath}`);
    // console.log(result);
    redirectPath = vaultPath.split('/');
    redirectPath.splice(-1,1);
    redirectPath = redirectPath.join('/');
    redirectPath = `${redirectPath}/`;
    if (result.isError) {
        req.flash('error_msg', JSON.stringify(result) || 'error');
    }
    res.redirect(`/vault/kv/secret/read/${encodeURIComponent(redirectPath)}`);
}));

//Edit Secret - View
router.get('/kv/secret/edit/:vaultPath', utility.asyncHandler(async (req, res, next) => {

    let vaultPath = req.params.vaultPath;
    //Vault API path may differ from actual Vault path (KV V2: Writing and reading are prefixed with the data/ path)
    let vaultApiPath = vaultPath;
    // console.log('vaultPath', vaultPath);

    let kvEnginePath = vaultPath.split('/')[0];
    let kvEngineConfig = await vault.read(`sys/internal/ui/mounts/${kvEnginePath}`);

    if (kvEngineConfig.isError) {
        req.flash('error_msg', JSON.stringify(kvEngineConfig) || 'error');
        res.redirect('/vault');
        return;
    }

    if (kvEngineConfig.data.options.version == 2) {
        let temp = vaultPath.split('/');
        temp.splice(1, 0, 'data');
        vaultApiPath = temp.join('/')
    }
    let result = await vault.read(vaultApiPath);
    // console.log('result', result);
    if (!result.isError) {
        let data = {};
        let metadata = null;
        if (kvEngineConfig.data.options.version == 2) {
            data = result.data.data;
            metadata = result.data.metadata;
        }
        else
            data = result.data;
        res.render('./vault/vault-kv-secret-edit', { user: req.user, vaultPath: vaultPath, secretData: data, secretMetaData: metadata });
    }
    else {
        req.flash('error_msg', JSON.stringify(result) || 'error');
        res.redirect('/vault');
    }

}));

//Edit Secret
router.post('/kv/secret/edit', utility.asyncHandler(async (req, res, next) => {

    const { vaultPath, secretData } = req.body;

    let vaultReqBody = {};

    secretData.forEach(secret => {
        if (secret.key)
            vaultReqBody[secret.key] = secret.value;
    });

    let vaultApiPath = `${vaultPath}`;
    // console.log(vaultReqBody);
    let result = await vault.write(vaultApiPath, vaultReqBody);
    // console.log(result);
    if (!result.isError) {
        res.redirect(`/vault/kv/secret/read/${encodeURIComponent(vaultPath)}`);
    }
    else {
        req.flash('error_msg', JSON.stringify(result) || 'error');
        res.redirect(`/vault/kv/secret/edit/${encodeURIComponent(vaultPath)}`);
    }

}));

module.exports = router;
