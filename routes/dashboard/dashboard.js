const express = require('express');
var request = require('request');
const router = express.Router();
const Train = require('../../models/Train');
const { ensureAuthenticated } = require('../../validation/auth');
const { getAgentOptions } = require('../../dind-certs-client');
const axios = require('axios');
const { response } = require('express');
const { vault } = require('../../utils');


// Dashboard
// Get Docker Information
function getAdminAuthRequestOptions() {
    let authServer = new URL('/auth/realms/pht/protocol/openid-connect/token', `https://${process.env.AUTH_SERVER_ADDRESS}`);
    authServer.port = process.env.AUTH_SERVER_PORT;
    var options = {
        url: authServer.toString(),
        headers: {
            'Content-Type': 'application/json',
        },
        form: {
            grant_type: "password",
            client_id: [Censored],
            username: [Censored],
            password: [Censored],
            scope: "openid profile email offline_access",
        }
    };
    return options;
}

router.get('/', ensureAuthenticated,
    function (req, res, next) {
        if (!req.harbor) {
            req.harbor = { auth: {} };
        }
        let options = getAdminAuthRequestOptions();
        request.post(options, (error, response) => {
            if (error) {
                // Describe the occuring problem better
                if (error.message.includes('EAI_AGAIN')) {
                    error.message += ' This means that the DNS resolution for the domain menzel.informatik.rwth-aachen.de ist NOT successfull. Most likely, the container has problems accessing the internet. Confirm this by using the command docker exec -it stationdeploymentfiles-pht-web /bin/bash and execute apt update. When the update is not successfull, the container has no working internet connection.'
                }
                console.log("error: ", error.message);
                req.flash('error_msg', error.message);
                res.redirect('/error');
                return;
            }

            if (response.statusCode == 200 && typeof(response.body) != 'undefined' && response.body != null){
                let body = JSON.parse(response.body);
                req.harbor.auth.admin_access_token = body.access_token;
                return next();
            } else {
                console.log("error: ", "Cannot Connect to the central authentication server.")
                req.flash('error_msg', "Cannot Connect to the central authentication server.");
                res.redirect('/error');
                return;
            }
        });
    },
    function (req, res) {
        let options = {
            url: `https://${process.env.CENTRALSERVICE_ADDRESS}:${process.env.CENTRALSERVICE_PORT}/${process.env.CENTRALSERVICE_ENDPOINT}/api/jobinfo/${process.env.STATION_ID}`,
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${req.harbor.auth.admin_access_token}`,
            },
        };
        request.get(options, function (error, response, body) {
            if (error) {
                console.log("error: ", error.message)
                req.flash('error_msg', error.message);
                res.redirect('/error');
                return;
            }

            console.log(body);

            try {
                let trains = JSON.parse(body);
                res.render('dashboard', { user: req.user, trains: trains });
            } catch (error) {
                console.error(error);
                console.log("error: ", "Cannot Connect to the central service.")
                req.flash('error_msg', "Cannot Connect to the central service.");
                res.redirect('/error');
                return;
            }

        });
    });

router.get('/images', ensureAuthenticated, (req, res) => {
    Train.find({}, function (err, docs) {
        if (err) {
            req.flash('error_msg', err.message);
            res.redirect('/dashboard');
        } else if (docs.length > 0) {

            let options = {
                uri: `https://${process.env.DOCKER_HOST}:${process.env.DOCKER_PORT}/images/json?all=1`,
                method: 'GET',
                agentOptions: getAgentOptions()
            };

            request(options, function (error, response, body) {
                if (error) {
                    console.log(error);
                    req.flash('error_msg', error.message);
                    res.redirect('/dashboard');
                } else {
                    let data = JSON.parse(body);
                    let pull_images = [];
                    let push_images = [];
                    for (var i = 0; i < data.length; i++) {
                        if (data[i].RepoTags == null) {
                            continue;
                        }
                        for (var j = 0; j < data[i].RepoTags.length; j++) {
                            if (data[i].RepoTags[j] == 'none') {
                                continue;
                            }
                            for (var k = 0; k < docs.length; k++) {
                                if (((docs[k].trainstoragelocation + ':' + docs[k].currentstation) === data[i].RepoTags[j]) && (data[i].ParentId === '' || (data[i].Labels && data[i].Labels.decrypted))) {
                                    pull_images.push({ Id: data[i].Id, RepoTag: data[i].RepoTags[j], JobId: docs[k].jobid, TrainClassId: docs[k].trainclassid, Labels: JSON.stringify(data[i].Labels) });
                                    break;
                                }
                                if (((docs[k].trainstoragelocation + ':' + docs[k].nextstation) === data[i].RepoTags[j])
                                    || (((docs[k].trainstoragelocation + ':' + docs[k].currentstation) === data[i].RepoTags[j]) && (data[i].ParentId !== ''))) {
                                    push_images.push({ Id: data[i].Id, RepoTag: data[i].RepoTags[j], TrainClassId: docs[k].trainclassid });
                                    break;
                                }
                            }
                        }
                    }
                    res.render('dashboard-images', { user: req.user, pull_images: pull_images, push_images: push_images });
                }
            });
        }
        else {
            res.render('dashboard-images', { user: req.user, pull_images: [], push_images: [] });
        }
    });

});

router.get('/containers', ensureAuthenticated, (req, res) => {
    Train.find({}, function (err, docs) {
        if (err) {
            req.flash('error_msg', err.message);
            res.redirect('/dashboard');
        } else if (docs.length > 0) {
            
            let options = {
                uri: `https://${process.env.DOCKER_HOST}:${process.env.DOCKER_PORT}/containers/json?all=1`,
                method: 'GET',
                agentOptions: getAgentOptions()
            };

            request(options, function (error, response, body) {
                if (error) {
                    req.flash('error_msg', error.message);
                    res.redirect('/dashboard');
                } else {
                    let data = JSON.parse(body);
                    let containers = [];
                    for (let i = 0; i < data.length; i++) {
                        for (let j = 0; j < data[i].Names.length; j++) {
                            console.log(data[i].Names[j]);
                            for (var k = 0; k < docs.length; k++) {
                                if ('/' + docs[k].jobid === data[i].Names[j]) {
                                    containers.push({ Id: data[i].Id, Name: data[i].Names[j], JobId: docs[k].jobid, Image: data[i].Image, State: data[i].State, Status: data[i].Status, NextTag: docs[k].nextstation, Repo: docs[k].trainstoragelocation, TrainClassId: docs[k].trainclassid });
                                    break;
                                }
                            }
                        }
                    }
                    res.render('dashboard-containers', { user: req.user, containers: containers});
                }
            });
        } else {
            res.render('dashboard-containers', { user: req.user, containers: []});
        }
    });
});

router.get('/reject', ensureAuthenticated, (req, res) => {
    const { jobid } = req.query;
    res.render('dashboard-reject', { user: req.user, jobid: jobid })
});

router.get('/create/container', ensureAuthenticated, (req, res) => {

    const { jobId, image, labels } = req.query;

    let envVariableList = [];

    // console.log(labels);
    if (labels) {
        let parsedLabels = JSON.parse(labels);
        // console.log(parsedLabels);
        if (parsedLabels && parsedLabels.envs)
            envVariableList = JSON.parse(parsedLabels.envs);
        // console.log(envVariableList);
    }

    // Validate envs structure
    const input_type_list = ["number", "password", "text", "url", "select"];
    let index = envVariableList.length
    while (index--) {
        const element = envVariableList[index];
        if (!element.name) {
            envVariableList.splice(index, 1);
            continue;
        }

        if (!input_type_list.includes(element.type)) {
            envVariableList[index].type = "text";
        }

        if (!element.required) {
            envVariableList[index].required = false;
        }
    }

    const blank_default = { name: "", type: "text", required: false };
    if (envVariableList.length > 0)
        envVariableList = envVariableList.concat([blank_default, blank_default, blank_default]);
    else
        envVariableList = envVariableList.concat([blank_default, blank_default, blank_default, blank_default, blank_default, blank_default]);


    // key-value version 1 - START
    let kv1Promise = new Promise((resolve) => {
        //Get key-value engines (v1)
        vault.getKeyValueEngines(version = 1).then(result => {
            let promises = [];
            let kvEnginesVersion1 = result;
            kvEnginesVersion1.forEach(kvEngine => {

                promises.push(new Promise((resolve) => {

                    //Traverse all existing paths
                    vault.traverse([kvEngine], (paths) => {
                        resolve(paths);
                    })

                }));

            });

            //When all results received
            Promise.all(promises).then(results => {

                let paths = results.flat();
                //Read keys corresponding to each path
                let pathPromises = paths.map(path => vault.read(path));
                Promise.all(pathPromises).then(results => {

                    let pathList = [];

                    for (let index = 0; index < results.length; index++) {
                        if (!results[index])
                            continue;
                        const element = results[index];
                        //key-value pairs
                        const data = element.data;
                        pathList.push(Object.keys(data).map(key => { return { path: paths[index], key: key } }));
                    }

                    pathList = pathList.flat();
                    resolve(pathList);

                })
            });
        });
    });
    // key-value version 1 - END

    // key-value version 2 - START
    let kv2Promise = new Promise((resolve) => {
        //Get key-value engines (v2)
        vault.getKeyValueEngines(version = 2).then(result => {

            let promises = [];
            let kvEnginesVersion2 = result;
            kvEnginesVersion2.forEach(kvEngine => {

                promises.push(new Promise((resolve) => {

                    //Traverse all existing paths (add 'metadata' for api calls)
                    vault.traverse([`${kvEngine}metadata/`], (paths) => {
                        resolve(paths);
                    })

                }));

            });

            //When all results received
            Promise.all(promises).then(results => {

                let paths = results.flat();

                //Replace 'metadata' with 'data' for api calls
                paths = paths.map(path => {
                    path = path.split("/");
                    path.splice(1, 1, "data");
                    path = path.join("/");
                    return path;
                });
                // console.log(paths);
                //Read keys corresponding to each path
                let pathPromises = paths.map(path => vault.read(path));
                Promise.all(pathPromises).then(results => {

                    let pathList = [];

                    for (let index = 0; index < results.length; index++) {
                        if (!results[index])
                            continue;
                        const element = results[index];
                        //key-value pairs
                        const data = element.data.data;
                        pathList.push(Object.keys(data).map(key => { return { path: paths[index], key: `data.${key}` } }));
                    }

                    pathList = pathList.flat();
                    resolve(pathList);

                })
            });

        });
    });
    // key-value version 2 - END

    Promise.all([kv1Promise, kv2Promise]).then(results => {
        let pathList = results.flat();
        res.render('dashboard-create-container', { user: req.user, image: image, jobId: jobId, paths: pathList, envVariableList: envVariableList });
    });

});

router.get('/notifications', ensureAuthenticated, (req, res) => {
    res.render('dashboard-notifications', { user: req.user });
});

router.get('/metadata', ensureAuthenticated, async (req, res) => {
    const host = process.env.METADATAPROVIDER_ENDPOINT || "http://metadataprovider:9988";
    var descriptionList = [];
    var list, useAllowList, stationIdentifier, ready;
    var succeeded = true  
    const t1 = axios.get(host + '/descriptionList').then(response => {
        descriptionList = response.data['descriptionList'];
    }).catch(err => {
        console.log(err)
        succeeded = false
    })
    const t2 = axios.get(host + '/filter').then(response => {
        list = response.data['list'];
        useAllowList = response.data['useAllowList'];
        console.log("USE ALLOW LIST RESPONSE: " + String(useAllowList))
    }).catch(err => {
        console.log(err)
        succeeded = false
    })
    const t3 = axios.get(host + '/configuration').then(response => {
        stationIdentifier = response.data['stationIdentifier']
        ready = response.data['ready']
    }).catch(err => {
        console.log(err)
        succeeded = false
    })
    await t1;
    await t2;
    await t3;
    if (succeeded) {
        res.render('metadata-list', { descriptionList, list: list.join('\n') , useAllowList: Boolean(useAllowList), user: req.user, stationIdentifier, ready });
    } else {
        console.warn('Error while connecting to metadata provider!')
        res.render('./partials/error', {
            errormessage: 'Error while connecting to metadata provider!',
            // For the navbar
            active: 'None',
        });
    }
    
})

module.exports = router;
