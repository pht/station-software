const express = require('express');
const router = express.Router();
const jwt = require('jsonwebtoken');
const passport = require('passport');
const bcrypt = require('bcryptjs');
// Load User model
const User = require('../../models/User');

router.post('/signin', (req, res) => {
    const { email, password } = req.body;
    // Match user
    User.findOne({
        email: email
    }).then(user => {
        if (!user) {
            res.status(401).send({ success: false, msg: 'Authentication failed. User not found.' });
        } else {
            // Match password
            bcrypt.compare(password, user.password, (err, isMatch) => {
                if (isMatch && !err) {
                    // if user is found and password is right create a token
                    var token = jwt.sign(user.toJSON(), process.env.JWT_SECRET, {
                        expiresIn: 604800 // 1 week
                    });
                    // return the information including token as JSON
                    res.json({ success: true, token: 'Bearer '+token });
                } else {
                    res.status(401).send({ success: false, msg: 'Authentication failed. Wrong password.' });
                }
            });
        }
    });
});

router.get('/protected', passport.authenticate('jwt', { session: true }),
    function(req, res) {
        res.send("get protected resource...");
    }
);
module.exports = router;