const EventEmitter = require('events')
const { envFileToDictionary } = require('./utils/envFileParser') 


// attributes needed in the env file:
// "MONGO_HOST", "MONGO_PORT", "MONGO_PORT", "MONGO_PASSWORD", "MONGO_DB", "CENTRALSERVICE_ADDRESS", "STATION_NAME"
const env_needed_attributes = ["STATION_ID", "HARBOR_USER", "HARBOR_PASSWORD", "HARBOR_EMAIL", "HARBOR_WEBHOOK_SECRET"]
// the configuration manager is accessed by the route handler and holds all the configuration data
// Additionally it has a method for indicating that the configuraiton is done and the programm can be killed
// this method is awaitable
eventEmitter = new EventEmitter() 

const configurationManager = {
    envconfiguration: "",
    ee: eventEmitter,
    configurationEnd: async function () {
        return new Promise((res,rej) => {
            eventEmitter.on('configurationFinished', () => {
                res(true)
            })
        })
    },
    updateHarborPassword: function (password) {
        this.envconfiguration = this.envconfiguration.replace(/HARBOR_PASSWORD=.*/, "HARBOR_PASSWORD=" + password)
    },
    getHarborUsernameAndPassword: function () {
        envDict = envFileToDictionary(this.envconfiguration)
        return {
            'username': envDict['HARBOR_USER'],
            'password': envDict['HARBOR_PASSWORD']
        }
    },
    configurationDidEnd: async function () {
        eventEmitter.emit('configurationFinished')
    },
    envconfigurationSanityCheck: async function () {
        try {
            const dict = envFileToDictionary(this.envconfiguration)
            for (const key of env_needed_attributes) {
                if (typeof dict[key] !== "string") {
                    return Promise.reject("Key " + String(key) + " not valid in envfile")
                }
            }
            return Promise.resolve()
        } catch (e) {
            console.log(e)
            return Promise.reject(e)
        }
    },
    password_reseted: false,
    publicKey: "",
    privateKey: ""
}
module.exports = configurationManager