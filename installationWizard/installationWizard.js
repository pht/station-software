// exports factory 

const router = require('express').Router()
const fileUpload = require('express-fileupload');
const controllerFactory = require('./controller')

const PAGES = ['installationWizard/page0','installationWizard/page1','installationWizard/page2','installationWizard/page3','installationWizard/page4']
module.exports = (configurationManager) => {

    const controller = controllerFactory(configurationManager)

    
    router.get('/wizard', controller.wizardViewController)

    router.use('/confupload', fileUpload())
    router.post('/confupload', controller.confupload)

    router.post('/otp', controller.setOtp)
 

    router.get('/messagehook', controller.messagehook)

    router.post('/HarborPassword', controller.setHarborPassword)

    router.post('/generateKeys', controller.generateKeys)

    router.post('/keys', controller.submitKeys)

    router.post('/complete', controller.configComplete)

    return router
}